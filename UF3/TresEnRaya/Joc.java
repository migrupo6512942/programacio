package Modular.TresEnRaya;
import java.util.Scanner;

public class Joc {

	static Scanner src = new Scanner(System.in);
	
	public static void jugarPartida(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		//Variables necessaries
		char[] tablero = {'_','_','_','_','_','_','_','_','_'};
		int tirada = 0;
		
		
				
		//Comença la parida
		
		boolean acabada = false;

		do {
			muestraTablero(tablero);
			char ficha = tirada%2==0? 'X':'O';
			String jugador = tirada%2==0? jugador1:jugador2;
			JuegaJugador(ficha, tablero, jugador);
			
			acabada = ComprobarFinal(tablero, jugador);
			if (acabada) {System.out.println("Enhorabuena " + jugador + " has ganado !");
					Jugador.historialPartidas(jugador);}
			tirada++;
		} while(!acabada);
}
		
		
	private static boolean ComprobarFinal(char[] tablero, String jugador) {
		// TODO Auto-generated method stub
		if (tablero[0] == tablero[1] && tablero[0] == tablero[2] && tablero[0] != '_') {
			return true;
		}
		else if (tablero[3] == tablero[4] && tablero[3] == tablero[5] && tablero[3] != '_') {
			return true;
		}
		else if (tablero[6] == tablero[7] && tablero[6] == tablero[8] && tablero[6] != '_') {
			return true;
		}
		
		
		else if (tablero[0] == tablero[3] && tablero[0] == tablero[6] && tablero[0] != '_') {
			return true;
		}
		else if (tablero[1] == tablero[4] && tablero[1] == tablero[7] && tablero[1] != '_') {
			return true;
		}
		else if (tablero[2] == tablero[5] && tablero[2] == tablero[8] && tablero[2] != '_') {
			return true;
		}
		
		
		else if (tablero[0] == tablero[4] && tablero[0] == tablero[8] && tablero[0] != '_') {
			return true;
		}
		else if (tablero[2] == tablero[4] && tablero[2] == tablero[6] && tablero[2] != '_') {
			return true;
		}
		
		else {return false;}
	}

	private static void JuegaJugador(char ficha, char[] tablero, String jugador) {
		// TODO Auto-generated method stub
		System.out.println("En que posicion quieres jugar " + jugador + "?");
		int pos = src.nextInt();
		
		tablero[pos] = ficha;
	}

	private static void muestraTablero(char[] tablero) {
		// TODO Auto-generated method stub
		
		System.out.println(tablero[0] + "|" + tablero[1] + "|" + tablero[2]);
		System.out.println(tablero[3] + "|" + tablero[4] + "|" + tablero[5]);
		System.out.println(tablero[6] + "|" + tablero[7] + "|" + tablero[8]);
	}
}