package Modular.TresEnRaya;

/*
 * Clase que gestiona la documentacio de com es juga al 3 en raya
 */

public class Ajuda {

	public static void unaAjuda() {
		// TODO Auto-generated method stub
		 System.out.print("Bienvenido al juego 3 en raya. Cada jugador marca con su símbolo (X o O) en el tablero.");
		 System.out.print("El objetivo es lograr una línea de tres de sus símbolos, ya sea horizontal, vertical o diagonal.");
	}
}
