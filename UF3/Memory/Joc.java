package Modular.Memory;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;


public class Joc {

	static Scanner src = new Scanner(System.in);
	
	public static void jugarPartida(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		//Variables necessaries
		char[][] taulerHide = new char [4][4];
		char[][] taulerOk = new char [4][4];
		int tirada = 0;

				
		//Comença la parida
		boolean acabada = false;
		
		generaTaulerOk(taulerOk);
		
		do {
			
			mostraTauler(taulerHide);
			JugarTorn(jugador1, jugador2, tirada, taulerHide);
			tirada++;
		} while(!acabada);
		
	}

	private static void generaTaulerOk(char[][] taulerOk) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		ArrayList<Character> LletrasEstan = new ArrayList <Character>();

		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				taulerOk[i][j] = '?';
			}
		}

		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				int fil;
				int col;
				char lletra;

				for( lletra = 'A'; lletra <= 'Z'; lletra++) {
					LletrasEstan.add(lletra);
				}
				
				do {
					fil =(int) rnd.nextInt(4);
					col =(int) rnd.nextInt(4);
					
				}while(taulerOk[fil][col] != '?');
				
				taulerOk[fil][col] = (char) lletra;
				
				do {
					fil =(int) rnd.nextInt(4);
					col =(int) rnd.nextInt(4);
					
				}while(taulerOk[fil][col] != '?');
				
				LletrasEstan.add((char)lletra);
				taulerOk[fil][col] = (char) lletra;

			}
		}
		
		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(taulerOk[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void JugarTorn(String jugador1, String jugador2, int tirada, char[][] tauler) {
		// TODO Auto-generated method stub
		String jugador = tirada%2==0? jugador1:jugador2;
		System.out.println("Quina posició 1 vols destapar " + jugador + " ?");
		System.out.print("X: "); 
		int x1 = src.nextInt();
		System.out.print("Y: "); 
		int y1 = src.nextInt();

	}

	private static void mostraTauler(char[][] tauler) {
		// TODO Auto-generated method stub
		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				tauler[i][j] = '?';
				System.out.print(tauler[i][j]);
			}
			System.out.println();
		}
	}
}