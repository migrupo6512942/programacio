package Modular.Memory;

import java.util.Hashtable;
import java.util.Scanner;

/**
 * Classe que gestiona els jugadors del penjat:
 * - alta de jugador
 * - baixa de jugador
 * - actualitzar partides guanyades
 * - consultar partides guanyades
 */

/*
 * Ficarem els jugadors en un diccionari.
 * Els mètodes més usuals són:
 * myMap.size(); // Devuelve el numero de elementos del Map
 * myMap.isEmpty(); // Devuelve true si no hay elementos en el Map y false si si los hay
 * myMap.put("68274736E", "Paco Soria"); // Añade un elemento al Map
 * myMap.get("68274736E"); // Devuelve el valor de la clave que se le pasa como parámetro o 'null' si la clave no existe
 * myMap.clear(); // Borra todos los componentes del Map
 * myMap.remove("68274736E"); // Borra el par clave/valor de la clave que se le pasa como parámetro
 * myMap.containsKey("68274736E"); // Devuelve true si en el map hay una clave que coincide con K
 * myMap.containsValue("Paco Soria"); // Devuelve true si en el map hay un Valor que coincide con V
 * myMap.values(); // Devuelve una "Collection" con los valores del Map
 */
public class Jugador {
	
	
	//Creació del diccionari amb els jugadors. Cada element serà (Nom jugador, núm partides guanyades)
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	//Necessitem el canal d'entrada de dades
	static Scanner src = new Scanner(System.in);
	
	/**
	 * Demana el nom del jugador
	 * Dóna d'alta un nou jugador a l'aplicació amb el comptador de partides guanyades a zero.
	 * Si el jugador ja està donat d'alta no farà res
	 * @return String amb el nom del jugador 
	 */
	public static String definirJugador1() {
		// TODO Auto-generated method stub
	
		String j1;
		System.out.print("Indica el nom del j1: ");
		j1 = src.nextLine();
		if (!jugadors.containsKey(j1)) 
			jugadors.put(j1, 0);
		
		return j1;
	}
	
	public static String definirJugador2() {
		// TODO Auto-generated method stub
	
		String j2;
		System.out.print("Indica el nom del j2: ");
		j2 = src.nextLine();
		if (!jugadors.containsKey(j2)) 
			jugadors.put(j2, 0);
		
		return j2;
	}

	public static void historialPartidas(String jugador) {
		// TODO Auto-generated method stub
		jugadors.put(jugador, jugadors.get(jugador)+ 1);
	}

	public static void VeureResultats() {
		// TODO Auto-generated method stub
		System.out.println(jugadors);
	}
	
}