package Modular.Memory.ElenaV1;

import java.util.Hashtable;
import java.util.Scanner;

/**
 * Classe que gestiona els jugadors del penjat:
 * - alta de jugador
 * - baixa de jugador
 * - actualitzar partides guanyades
 * - consultar partides guanyades
 */

public class Jugador {
	
	
	//Creació del diccionari amb els jugadors. Cada element serà (Nom jugador, núm partides guanyades)
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	//Necessitem el canal d'entrada de dades
	static Scanner src = new Scanner(System.in);
	
	public static String definirJugador() {
		// TODO Auto-generated method stub
	
		String j1;
		System.out.print("Indica el nom del jugador: ");
		j1 = src.nextLine();
		if (!jugadors.containsKey(j1)) 
			jugadors.put(j1, 0);
		
		return j1;
	}
	

	public static void historialPartidas(String jugador) {
		// TODO Auto-generated method stub
		jugadors.put(jugador, jugadors.get(jugador)+ 1);
	}

	public static void VeureResultats() {
		// TODO Auto-generated method stub
		System.out.println(jugadors);
	}
	
}