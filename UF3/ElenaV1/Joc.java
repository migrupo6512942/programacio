package Modular.Memory.ElenaV1;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;


public class Joc {

	static Scanner src = new Scanner(System.in);
	
	public static void jugarPartida(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		//Variables necessaries
		char[][] oculta;
		char[][] visible;
		final int max = 4;
		int puntsj1, puntsj2;
		int tirada;
		boolean acaba;

	
	// iniciar partida
	
	oculta = posarlletres(max);
	visible = creartauler(max);
	puntsj1 = puntsj2 = 0;
	acaba = false;
	tirada = 1;
	int fila1, col1, fila2, col2;
	
	// jugar partida
	
	do {
		
		mostrarEstat(visible, puntsj1, puntsj2, tirada, max, jugador1, jugador2);
		System.out.println("Quina casella vols destapar ? ");
		fila1 = mirarFila();
		col1 = mirarCol();
		destaparCasella1(max, oculta, visible, fila1, col1);
		System.out.println("Quina casella vols destapar ? ");
		fila2 = mirarFila();
		col2 = mirarCol();
		destaparCasella(max, oculta, visible,fila1, col1, fila2, col2);
		
		tirada = actualitzarTauler(fila1, fila2, col1, col2, oculta, visible, max, tirada, puntsj1, puntsj2);
		acaba = comprobarTauler(puntsj1, puntsj2);
		comprobarGuanyador(puntsj1, puntsj2, jugador1, jugador2);
		
	}while(acaba);
	
	mostrarVisible(visible, puntsj1, puntsj2, tirada, max);
	
	}

	private static void destaparCasella1(int max, char[][] oculta, char[][] visible, int fila1, int col1) {
		// TODO Auto-generated method stub
			boolean verifico = true;
			
			do {
			
				if(visible[fila1][col1] == '?') {
					visible[fila1][col1] = oculta[fila1][col1];
				}
				else {
					verifico = false;
					System.out.println("Aquesta casella ya ha estat destapada");
				}
			}while(verifico);
		
			Joc.mostrarEstat(visible, col1, fila1, col1, max, null, null);
	}


	private static void mostrarVisible(char[][] visible, int puntsj1, int puntsj2, int tirada, int max) {
		// TODO Auto-generated method stub
		
		for(int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				visible[i][j] = '_';
			}
		}
		
		System.out.println("El jugador1 te " + puntsj1 + " punts");
		System.out.println("El jugador2 te " + puntsj2 + " punts");
		System.out.println("Numero de torns " + tirada);

	}

	private static void comprobarGuanyador(int puntsj1, int puntsj2, String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		if(puntsj1 > puntsj2) {
			System.out.println("Has guanyat " + jugador1 + "!");
		}else if(puntsj1 < puntsj2)   {System.out.println("Has guanyat " + jugador2 + "!");}
		else {System.out.println("Empat !");}
	}

	private static boolean comprobarTauler(int puntsj1, int puntsj2) {
		// TODO Auto-generated method stub
		boolean acabar = true;
		
		if(puntsj1 + puntsj2 == 8) {
			acabar = false;
		}
		
		return acabar;
	}

	private static int actualitzarTauler(int fila1, int fila2, int col1, int col2, char[][] oculta, char[][] visible,
			int max, int tirada, int puntsj1, int puntsj2) {
		// TODO Auto-generated method stub
		boolean correcto = true;
		
		if(visible[fila1][col1] != visible[fila2][col2]) {
			visible[fila1][col1] = '?';
			visible[fila2][col2] = '?';
			correcto = false;
		}
		
		if(correcto) {
			char turno = tirada%2==0? '1':'2';
			if(turno == '1') {
				puntsj1 = puntsj1 +1;
			}
			else {puntsj2 = puntsj2 +1;}
		}
		
		tirada++;
		
		return tirada;
	}

	private static void destaparCasella(int max, char[][] oculta, char[][] visible, int fila1, int col1, int fila2, int col2) {
		// TODO Auto-generated method stub
		boolean verifico = true;
		
		do {
		
			if(visible[fila1][col1] == '?') {
				visible[fila1][col1] = oculta[fila1][col1];
			}
			else if(visible[fila2][col2] == '?') {
				visible[fila2][col2] = oculta[fila2][col2];
			}
			else {
				verifico = false;
				System.out.println("Aquesta casella ya ha estat destapada");
			}
		}while(verifico);
	
		Joc.mostrarEstat(visible, col1, fila1, col1, max, null, null);
	}

	private static int mirarCol() {
		// TODO Auto-generated method stub
		int col;
		boolean correcto = false;
		
		do {
			
			System.out.println("Y: ");

			col = src.nextInt();
			
			if(col <= 3 && col >= 0) {
				correcto = true;
			}else {System.out.print("Error d'entrada");}
			
		}while(correcto = false);
		
		return col;
	}

	private static int mirarFila() {
		// TODO Auto-generated method stub
		int fila;
		boolean correcto = false;

		do {
			System.out.println("X: ");

			fila = src.nextInt();
			
			if(fila <= 4 && fila >= 0) {
				correcto = true;
			}else {System.out.print("Error d'entrada");}
			
		}while(correcto = false);
		
		return fila;
	}

	private static void mostrarEstat(char[][] visible, int puntsj1, int puntsj2, int tirada, int max, String jugador1,
			String jugador2) {
		// TODO Auto-generated method stub
		System.out.println("Estat del Tauler al torn " + tirada + " : ");
		
		for(int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				System.out.print(visible[i][j]);
			}
			System.out.println();
		}
		
		System.out.println("El jugador " + jugador1 + " te " + puntsj1 + " punts ");
		System.out.println("El jugador " + jugador2 + " te " + puntsj2 + " punts ");
		
	}

	private static char[][] creartauler(int max) {
		// TODO Auto-generated method stub
		char[][] visible = new char [max][max];
		
		for(int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				visible[i][j] = '?';
			}
		}
		
		return visible;
	}

	private static char[][] posarlletres(int max) {
		// TODO Auto-generated method stub		
		char[][]oculta = new char [max][max];
		char ascii = 65; //ASCII dec 65 = A  equival a   ascii='A';
		int comptador = 0;
		for (int F = 0; F < max; F++)
			for (int C = 0; C < max; C++)
			{
				if(comptador != 0 && (comptador%2) == 0)
					ascii++;

				oculta[F][C] = ascii;
				comptador++;
				
			}

		
		return oculta;
	}

}