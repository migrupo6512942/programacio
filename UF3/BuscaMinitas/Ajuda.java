package Modular.BuscaMinitas;

/**
 * Proporciona información y consejos sobre cómo jugar al Buscaminas.
 */
public class Ajuda {

    /**
     * Muestra una introducción al juego y un resumen de cómo se juega.
     */
    public static void mostrarAyuda() {
        System.out.print("En el Buscaminas, el objetivo es descubrir todas las casillas sin minas en un tablero cuadriculado, evitando hacer clic en las que tienen minas. Las cifras en las casillas revelan la cantidad de minas cercanas. Utiliza marcadores para señalar las casillas con minas y aplica lógica para evitarlas y revelar las casillas seguras.");
    }
}
