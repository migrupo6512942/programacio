package Modular.BuscaMinitas;

import java.util.Scanner;

/**
 * Clase que contiene la lógica del juego BuscaMinas.
 * 
 * This class contains the logic of the BuscaMinas game.
 * 
 * @author deivi
 */
public class Joc {

    static Scanner src = new Scanner(System.in);

    /**
     * Método principal que maneja la lógica de una partida de BuscaMinas.
     * 
     * @param mesures Un array de enteros que contiene las dimensiones y el número de minas.
     * @return true si el jugador gana la partida, false si pierde.
     */
    public static boolean jugarPartida(int[] mesures) {
        char[][] ocult;
        char[][] visible;
        int punts;
        int puntsAnt;
        int desfila = 0, descol = 0;
        boolean acaba;
        boolean bomba;
        boolean guanyar;
        int fila = mesures[0];
        int col = mesures[1];
        int minitas = mesures[2];
        int op = 0;

        punts = 1;
        puntsAnt = 1;
        acaba = false;
        bomba = false;
        guanyar = false;
        visible = taulerBuit(fila, col);
        ocult = visible;

        ocult = ComençarTauler(fila, col, minitas, desfila, descol, ocult, visible);
        visible[desfila][descol] = ocult[desfila][descol];
        if (visible[desfila][descol] == '-') {
            punts = destaparCascada(desfila, descol, visible, ocult, fila, col);
        }

        do {
            op = veureEstat(visible, punts, fila, col);
            switch (op) {
                case 1:
                    puntsAnt = punts;
                    punts = destaparCasella(visible, ocult, punts, fila, col);
                    if (punts == puntsAnt) {
                        bomba = true;
                    }
                    break;
                case 2:
                    marcarCasella(visible, fila, col);
                    break;
                case 3:
                    desmarcarCasella(visible, fila, col);
                    break;
            }

            if (!bomba) {
                acaba = comprobarFinal(punts, fila, col, minitas);
            }

        } while (!acaba && !bomba);

        guanyar = mostrarFinal(visible, punts, bomba, acaba, fila, col);

        return guanyar;
    }

    /**
     * Comienza el tablero y determina la primera casilla a destapar.
     */
    private static char[][] ComençarTauler(int fila, int col, int minitas, int desfila, int descol, char[][] ocult,
            char[][] visible) {
        desfila = destaparCaselleaPrincipi(fila, col);
        descol = destaparCaselleaPrincipi(fila, col);

        ocult = generarTauler(fila, col, minitas, desfila, descol);

        return ocult;
    }

    /**
     * Determina la primera posición a destapar.
     */
    private static int destaparCaselleaPrincipi(int fila, int col) {
        int pos = 0;
        System.out.println("Quina casella vols destapar: ");

        do {
            pos = src.nextInt();
        } while (pos < 0);

        return pos;
    }

    /**
     * Muestra el tablero final del juego una vez que se ha determinado si el jugador ha perdido o ganado.
     */
    private static boolean mostrarFinal(char[][] visible, int punts, boolean bomba, boolean acaba, int fila, int col) {
        boolean guanyar = false;

        System.out.println("Tauler final: ");

        // Imprimir encabezado de columnas
        System.out.print("  ");
        for (int j = 0; j < col; j++) {
            System.out.print(" " + j);
        }
        System.out.println();

        // Imprimir borde superior del tablero
        System.out.print(" +");
        for (int j = 0; j < col; j++) {
            System.out.print("--");
        }
        System.out.println("+");

        for (int i = 0; i < fila; i++) {
            // Imprimir número de fila a la izquierda
            System.out.print(i + "|");

            for (int j = 0; j < col; j++) {
                System.out.print(" " + visible[i][j]);
            }

            // Imprimir número de fila a la derecha
            System.out.println(" |" + i);
        }

        // Imprimir borde inferior del tablero
        System.out.print(" +");
        for (int j = 0; j < col; j++) {
            System.out.print("--");
        }
        System.out.println("+");

        // Imprimir números de columna en la parte inferior
        System.out.print("  ");
        for (int j = 0; j < col; j++) {
            System.out.print(" " + j);
        }
        System.out.println();

        if (bomba) {
            System.out.println("Has perdut");
        } else {
            System.out.println("Has guanyat");
            guanyar = true;
        }

        return guanyar;
    }

    /**
     * Comprueba si el jugador ha ganado la partida.
     */
    private static boolean comprobarFinal(int punts, int fila, int col, int minitas) {
        return punts == (fila * col) - minitas;
    }

    /**
     * Desmarca una casilla en el tablero visible.
     */
    private static void desmarcarCasella(char[][] visible, int fila, int col) {
        int x, y;

        do {
            System.out.println("Quina fila vols destapar: ");
            x = src.nextInt();
            System.out.println("Quina columna vols destapar: ");
            y = src.nextInt();
        } while (x <= 0 && y <= 0);

        if (visible[x][y] == 'F') {
            visible[x][y] = '-';
        } else {
            System.out.println("Aquesta casella no ha estat marcada");
        }
    }

    /**
     * Marca una casilla en el tablero visible.
     */
    private static void marcarCasella(char[][] visible, int fila, int col) {
        int x, y;

        do {
            System.out.println("Quina fila vols marcar: ");
            x = src.nextInt();
            System.out.println("Quina columna vols marcar: ");
            y = src.nextInt();
        } while (x <= 0 && y <= 0);

        visible[x][y] = 'F';
    }

    /**
     * Destapa una casilla en el tablero visible y maneja la lógica de la cascada.
     */
    private static int destaparCasella(char[][] visible, char[][] ocult, int punts, int fila, int col) {
        int x, y;
        int res = 0;

        do {
            System.out.println("Quina fila vols destapar: ");
            x = src.nextInt();
            System.out.println("Quina columna vols destapar: ");
            y = src.nextInt();
        } while (x < 0 || y < 0);

        if (ocult[x][y] == '✇') {
            visible[x][y] = ocult[x][y];
        } else {
            res = destaparCascada(x, y, visible, ocult, fila, col);
        }

        punts = punts + res;

        return punts;
    }

    /**
     * Destapa todas las casillas alrededor de un espacio vacío hasta encontrar números.
     */
    private static int destaparCascada(int x, int y, char[][] visible, char[][] ocult, int fila, int col) {
        int res = 0;

        if (ocult[x][y] != '-') {
            visible[x][y] = ocult[x][y];
            return 1;
        } else {
            visible[x][y] = ocult[x][y];

            if (x - 1 >= 0) {
                if (visible[x - 1][y] != ocult[x - 1][y]) {
                    res = 1 + destaparCascada(x - 1, y, visible, ocult, fila, col);
                }
            }
            if (x + 1 <= fila - 1) {
                if (visible[x + 1][y] != ocult[x + 1][y]) {
                    res = 1 + destaparCascada(x + 1, y, visible, ocult, fila, col);
                }
            }
            if (y - 1 >= 0) {
                if (visible[x][y - 1] != ocult[x][y - 1]) {
                    res = 1 + destaparCascada(x, y - 1, visible, ocult, fila, col);
                }
            }
            if (y + 1 <= col - 1) {
                if (visible[x][y + 1] != ocult[x][y + 1]) {
                    res = 1 + destaparCascada(x, y + 1, visible, ocult, fila, col);
                }
            }
            if (y - 1 >= 0 && x - 1 >= 0) {
                if (visible[x - 1][y - 1] != ocult[x - 1][y - 1]) {
                    res = 1 + destaparCascada(x - 1, y - 1, visible, ocult, fila, col);
                }
            }
            if (y + 1 <= col - 1 && x + 1 <= fila - 1) {
                if (visible[x + 1][y + 1] != ocult[x + 1][y + 1]) {
                    res = 1 + destaparCascada(x + 1, y + 1, visible, ocult, fila, col);
                }
            }
            if (y + 1 <= col - 1 && x - 1 >= 0) {
                if (visible[x - 1][y + 1] != ocult[x - 1][y + 1]) {
                    res = 1 + destaparCascada(x - 1, y + 1, visible, ocult, fila, col);
                }
            }
            if (y - 1 >= 0 && x + 1 <= fila - 1) {
                if (visible[x + 1][y - 1] != ocult[x + 1][y - 1]) {
                    res = 1 + destaparCascada(x + 1, y - 1, visible, ocult, fila, col);
                }
            }
        }

        return res;
    }

    /**
     * Muestra un menú de opciones para todas las acciones a realizar.
     */
    private static int mostrarOpciones() {
        int op = 0;
        boolean correcte = true;

        System.out.println("Que vols fer?");
        System.out.println("1. Destapar casella");
        System.out.println("2. Marcar casella");
        System.out.println("3. Desmarcar casella");
        System.out.println("0. Sortir");

        do {
            System.out.print("Opció: ");
            try {
                op = src.nextInt();
            } catch (Exception e) {
                System.out.println("Error, opció no vàlida");
                correcte = false;
            }

        } while (!correcte);

        return op;
    }

    /**
     * Muestra el estado del tablero después de cada acción.
     */
    private static int veureEstat(char[][] visible, int punts, int fila, int col) {
        System.out.println("Estat del tauler: ");

        // Imprimir encabezado de columnas
        System.out.print("  ");
        for (int j = 0; j < col; j++) {
            System.out.print(" " + j);
        }
        System.out.println();

        // Imprimir borde superior del tablero
        System.out.print(" +");
        for (int j = 0; j < col; j++) {
            System.out.print("--");
        }
        System.out.println("+");

        for (int i = 0; i < fila; i++) {
            // Imprimir número de fila a la izquierda
            System.out.print(i + "|");

            for (int j = 0; j < col; j++) {
                System.out.print(" " + visible[i][j]);
            }

            // Imprimir número de fila a la derecha
            System.out.println(" |" + i);
        }

        // Imprimir borde inferior del tablero
        System.out.print(" +");
        for (int j = 0; j < col; j++) {
            System.out.print("--");
        }
        System.out.println("+");

        // Imprimir números de columna en la parte inferior
        System.out.print("  ");
        for (int j = 0; j < col; j++) {
            System.out.print(" " + j);
        }
        System.out.println();

        return mostrarOpciones();
    }

    /**
     * Genera el tablero inicial oculto.
     */
    private static char[][] generarTauler(int fila, int col, int minitas, int desfila, int descol) {
        char[][] mat = new char[fila][col];

        for (int i = 0; i < fila; i++) {
            for (int j = 0; j < col; j++) {
                mat[i][j] = '-';
            }
        }

        mat = posarMines(mat, minitas, fila, col, desfila, descol);
        mat = posarNum(mat, fila, col);

        return mat;
    }

    /**
     * Pone los números alrededor de las minas.
     */
    private static char[][] posarNum(char[][] mat, int fila, int col) {
        for (int i = 0; i < fila; i++) {
            for (int j = 0; j < col; j++) {
                if (i - 1 >= 0) {
                    if (mat[i - 1][j] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }

                    }
                }
                if (i - 1 >= 0 && j - 1 >= 0) {
                    if (mat[i - 1][j - 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (i - 1 >= 0 && j + 1 < col) {
                    if (mat[i - 1][j + 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (j - 1 >= 0) {
                    if (mat[i][j - 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (j + 1 < col) {
                    if (mat[i][j + 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (i + 1 < fila) {
                    if (mat[i + 1][j] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (i + 1 < fila && j + 1 < col) {
                    if (mat[i + 1][j + 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
                if (i + 1 < fila && j - 1 >= 0) {
                    if (mat[i + 1][j - 1] == '✇') {
                        if (mat[i][j] != '✇') {
                            if (mat[i][j] == '-') {
                                mat[i][j] = '1';
                            } else {
                                mat[i][j]++;
                            }
                        }
                    }
                }
            }
        }

        return mat;
    }

    /**
     * Coloca las minas aleatoriamente en el tablero oculto.
     */
    private static char[][] posarMines(char[][] mat, int minitas, int fila, int col, int desfila, int descol) {
        do {
            int x = (int) (Math.random() * fila);
            int y = (int) (Math.random() * col);

            if (mat[x][y] == '-') {
                if (x != desfila && y != descol) {
                    mat[x][y] = '✇';
                    minitas--;
                }
            }

        } while (minitas > 0);

        return mat;
    }

    /**
     * Genera el tablero visible donde no se debe ver nada.
     */
    private static char[][] taulerBuit(int fila, int col) {
        char[][] mat = new char[fila][col];

        for (int i = 0; i < fila; i++) {
            for (int j = 0; j < col; j++) {
                mat[i][j] = '?';
            }
        }

        return mat;
    }
}
