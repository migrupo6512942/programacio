package Modular.BuscaMinitas;

import java.util.Scanner;
import java.lang.Math;

/**
 * Classe que llança l'aplicació del penjat. És un distribuidor de tasques
 * mitjançant un menú d'opcions
 * 
 * This class launches the BuscaMinas application. It acts as a task distributor
 * through a menu of options.
 * 
 * @author deivi
 */
public class Programa {

    static Scanner src = new Scanner(System.in);

    /**
     * Mètode principal que executa el programa del BuscaMinas.
     * 
     * @param args Arguments de la línia de comandes (no utilitzats en aquest cas).
     */
    public static void main(String[] args) {
        int op = 0; // guarda l'opció escollida per l'usuari
        String jugador = null; // guarda el nom del jugador actual
        boolean resultat;
        boolean definirJugador = false;
        int[] mesures = { 8, 8, 10 };

        do {
            op = mostrarMenú();

            switch (op) {
                case 1:
                    Ajuda.mostrarAyuda();
                    break;
                case 2:
                    jugador = Jugador.definirJugador();
                    definirJugador = true;
                    break;

                case 3:
                    if (definirJugador == true) {
                        mesures = definirDificultat();
                        definirJugador = true;
                    } else
                        System.out.println("No pots seleccionar la dificultat fins que no estigui definit el jugador");
                    break;

                case 4:
                    if (definirJugador == true) {
                        resultat = Joc.jugarPartida(mesures);
                        if (resultat) {
                            Jugador.sumarVictoria(jugador);
                        }
                        definirJugador = false; // obliguem a definir el jugador si volem jugar altra partida
                    } else
                        System.out.println("No pots jugar fins que no estigui defini la dificultat");
                    break;
                case 5:
                    System.out.println("Partides guanyades per cada jugador: ");
                    Jugador.verResultados();
                    break;
                case 0:
                    System.out.println("Fi de joc");
                    break;
                default:
                    System.out.println("Opció incorrecta");
            }

        } while (op != 0);

    }

    /**
     * Serveix seleccionar les dades per fer la matriu ja que van d'acord a la dificultat.
     * 
     * Selects the data to create the matrix based on the difficulty.
     * 
     * @return Un array d'enters que conté les dimensions i el nombre de mines.
     */
    public static int[] definirDificultat() {
        int[] vec = new int[3];

        System.out.println("Selecciona la teva dificultat: ");
        int df = mostrarMenúDif();;
        switch (df) {
            case 1:
                vec[0] = 8;
                vec[1] = 8;
                vec[2] = 10;
                break;
            case 2:
                vec[0] = 16;
                vec[1] = 16;
                vec[2] = 40;
                break;
            case 3:
                vec[0] = 16;
                vec[1] = 30;
                vec[2] = 99;
                break;
            case 4:
                System.out.println("Defineix amplada y altura del tauler: ");
                vec[0] = src.nextInt();
                vec[1] = src.nextInt();
                do {
                    System.out.println("Numero de mines (menor que el numero de caselles "
                            + Math.round((vec[0] * vec[1]) * 0.3) + ") : ");
                    vec[2] = src.nextInt();
                } while (vec[2] > Math.round((vec[0] * vec[1]) * 0.3));
                break;
        }

        return vec;
    }

    /**
     * Serveix per mostrar el menu d'opcions de les dificultats.
     * 
     * Displays the menu of difficulty options.
     * 
     * @return L'opció seleccionada pel usuari.
     */
    private static int mostrarMenúDif() {
        int df = 0;
        boolean correcte = true;

        System.out.println("Dificultats: ");
        System.out.println("1. Nivell principiant: 8 × 8 caselles i 10 mines.");
        System.out.println("2. Nivell intermig: 16 × 16 caselles i 40 mines.");
        System.out.println("3. Nivell expert: 16 × 30 caselles i 99 mines.");
        System.out.println("4. Nivell personalitzat: en aquest cas l'usuari personalitza el seu joc triant el nombre de mines i la mida de la graella");
        System.out.println("0. Sortir");

        df = comprobarDatos(correcte);

        return df;
    }

    /**
     * Serveix per comprovar l'entrada de dades.
     * 
     * Checks the input data.
     * 
     * @param correcte Indica si les dades són correctes o no.
     * @return El valor de l'opció verificada.
     */
    private static int comprobarDatos(boolean correcte) {
        int df = 0;

        do {
            System.out.print("Opció: ");
            try {
                df = src.nextInt();
            } catch (Exception e) {
                System.out.println("Error, opció no vàlida");
                src.nextLine();
                correcte = false;
            }

        } while (correcte == false);

        return df;
    }

    /**
     * Serveix per mostrar el menu d'opcions del menu principal.
     * 
     * Displays the menu of options for the main menu.
     * 
     * @return L'opció seleccionada pel usuari.
     */
    private static int mostrarMenú() {
        int op = 0;
        boolean correcte = true;

        System.out.println("BuscaMinas");
        System.out.println("1. Mostrar Ajuda");
        System.out.println("2. Definir jugador");
        System.out.println("3. Escollir dificultat");
        System.out.println("4. Jugar partida");
        System.out.println("5. Resultats jugador");
        System.out.println("0. Sortir");

        op = comprobarDatos(correcte);

        return op;
    }

}
