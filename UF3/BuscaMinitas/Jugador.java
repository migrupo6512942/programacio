package Modular.BuscaMinitas;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Gestiona la información de los jugadores y sus partidas.
 */
public class Jugador {

    // Creación del diccionario con los jugadores. Cada elemento será (Nombre jugador, número partidas ganadas)
    private static final Map<String, Integer> jugadores = new HashMap<>();
    // Necesitamos el canal de entrada de datos
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Permite que un jugador defina su nombre.
     *
     * @return El nombre del jugador.
     */
    public static String definirJugador() {
        System.out.print("Indica tu nombre: ");
        String nombreJugador = scanner.nextLine();

        // Si el jugador no está en el diccionario, lo añadimos con 0 partidas ganadas
        jugadores.putIfAbsent(nombreJugador, 0);

        return nombreJugador;
    }

    /**
     * Suma una victoria al jugador.
     *
     * @param jugador El nombre del jugador.
     */
    public static void sumarVictoria(String jugador) {
        jugadores.put(jugador, jugadores.get(jugador) + 1);
    }

    /**
     * Muestra los resultados de cada jugador.
     */
    public static void verResultados() {
        System.out.println(jugadores);
    }
}
