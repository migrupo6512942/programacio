package Modular.HundirFlota;

/*
 * Clase que gestiona la documentacio de com es juga al 3 en raya
 */

public class Ajuda {

	public static void unaAjuda() {
		// TODO Auto-generated method stub
		 System.out.print("Hundir la Flota es un juego de estrategia naval para dos jugadores. Cada jugador tiene su propio tablero, oculto al oponente, en el que coloca sus barcos. El objetivo del juego es hundir todos los barcos del oponente adivinando las coordenadas en las que están ubicados.");
	}
}
