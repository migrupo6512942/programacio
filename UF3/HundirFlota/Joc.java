package Modular.HundirFlota;
import java.util.Scanner;

public class Joc {

	static Scanner src = new Scanner(System.in);
	
	public static void jugarPartida(String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		
		//Variables necessaries
		char[][] ocultaJ1;
		char[][] visibleJ1;
		char[][] ocultaJ2;
		char[][] visibleJ2;
		final int max = 5;
		int puntsj1, puntsj2;
		int tirada;
		boolean acaba;

				
		//Comença la parida
		visibleJ1 = OmplirTaulerBuit(max);
		visibleJ2 = OmplirTaulerBuit(max);
		ocultaJ1 = visibleJ1;
		ocultaJ2 = visibleJ2;

		puntsj1 = puntsj2 = 0;
		acaba = false;
		tirada = 1;
		int fila1 = 0, col1 = 0;

		//Jugar
		
		do {
			
		VerEstado(visibleJ1, visibleJ2, jugador1, jugador2, puntsj2, puntsj1, max);
		System.out.println("Comença la partida");
		System.out.println(jugador1 + "Coloca una fragata al teu tauler");
		ColocarFragata(ocultaJ1, fila1, col1, max);
		System.out.println(jugador2 + "Coloca una fragata al teu tauler");
		ColocarFragata2(ocultaJ2, fila1, col1, max);
		
		System.out.println("Tirada" + tirada);
		System.out.println(jugador1 + "Dispara al tauler enemic");
		puntsj1 = Disparar1(ocultaJ2, visibleJ2, puntsj1, max);
		System.out.println(jugador2 + "Dispara al tauler enemic");
		puntsj2 = Disparar2(ocultaJ1, visibleJ1, puntsj2, max);
				
		}while(acaba == false);
		
		AcabarJoc(visibleJ1, visibleJ2, puntsj1, puntsj2, max);
		
	}

	private static void AcabarJoc(char[][] visibleJ1, char[][] visibleJ2, int puntsj1, int puntsj2, int max) {
		// TODO Auto-generated method stub
		
		System.out.println("Tauler J1");
		for(int i = 0; i < max; i++) {
			for(int j = 0; j < max; j++) {
				System.out.print(visibleJ1[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("Tauler J2");
		for(int i = 0; i < max; i++) {
			for(int j = 0; j < max; j++) {
				System.out.print(visibleJ2[i][j] + " ");
			}
			System.out.println();
		}
		
		if(puntsj1 > puntsj2) {
			System.out.println("Ha guanyat el jugador1 amb " + puntsj1);
		}
		else if (puntsj1 < puntsj2) {
			System.out.println("Ha guanyat el jugador2 amb " + puntsj2);
		}
		else {System.out.println("Empat");}
		
		
	}

	private static int Disparar2(char[][] ocultaJ1, char[][] visibleJ1, int puntsj2, int max) {
		// TODO Auto-generated method stub
		System.out.println("On vols disparar?");
		boolean correcte = false;
		boolean acaba = false;

		do {
			System.out.print("X: ");int fila1 = src.nextInt();
			System.out.print("Y: ");int col1 = src.nextInt();
			
			if(fila1 < max && col1 < max) {
				if(ocultaJ1[fila1][col1] == '-') {
					visibleJ1[fila1][col1] = 'W';
					correcte = true;
					System.out.println("Aigua");
				}
				else if(ocultaJ1[fila1][col1] == 'X') {
					visibleJ1[fila1][col1] = 'X';
					correcte = true;
					System.out.println("Tocat!");
					
					if(visibleJ1[fila1+1][col1] == 'X' | visibleJ1[fila1-1][col1] == 'X' | visibleJ1[fila1][col1+1] == 'X' | visibleJ1[fila1][col1-1] == 'X') {
						System.out.println("Tocat y fos!");
						puntsj2 ++;
					};
					
				}
			}
			
		}while(correcte = false);
		
		return puntsj2;
	}

	private static int Disparar1(char[][] ocultaJ2, char[][] visibleJ2, int puntsj1, int max) {
		// TODO Auto-generated method stub
		System.out.println("On vols disparar?");
		boolean correcte = false;

		do {
			System.out.print("X: ");int fila1 = src.nextInt();
			System.out.print("Y: ");int col1 = src.nextInt();
			
			if(fila1 < 10 && col1 < 10) {
				if(ocultaJ2[fila1][col1] == '-') {
					visibleJ2[fila1][col1] = 'W';
					correcte = true;
					System.out.println("Aigua");
				}
				else if(ocultaJ2[fila1][col1] == 'X') {
					visibleJ2[fila1][col1] = 'X';
					correcte = true;
					System.out.println("Tocat!");
					
					if(visibleJ2[fila1+1][col1] == 'X' | visibleJ2[fila1-1][col1] == 'X' | visibleJ2[fila1][col1+1] == 'X' | visibleJ2[fila1][col1-1] == 'X') {
						System.out.println("Tocat y fos!");
						puntsj1 ++;
					};
					
				}
			}
			
		}while(correcte = false);
		
		return puntsj1;
	}

	private static void ColocarFragata2(char[][] ocultaJ2, int fila1, int col1, int max) {
		// TODO Auto-generated method stub
		boolean correcte = false;
		int op = 0;
		
		do {
		System.out.print("X: ");fila1 = src.nextInt();
		System.out.print("Y: ");col1 = src.nextInt();
		
		if(fila1 < max && col1 < 10) {
			if(ocultaJ2[fila1][col1] == '-') {
				ocultaJ2[fila1][col1] = 'X';
				
				System.out.println("En quina direcció vols que vagi ?");
				
				do {
					op = mostrarMenú();

					switch (op) {
					case 1:
						fila1 = fila1 -1;
						
						if(fila1 < max && fila1 >= 0) {
							if(ocultaJ2[fila1][col1] == '-') {
								ocultaJ2[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					case 2:
						
						fila1 = fila1+1;
						
						if(fila1 < max && fila1 >= 0) {
							if(ocultaJ2[fila1][col1] == '-') {
								ocultaJ2[fila1][col1] = 'X';
								correcte =  true;
								}
						}	
						
						break;
					case 3:
						
						col1 = col1-1;
						
						if(col1 < max && col1 >= 0) {
							if(ocultaJ2[fila1][col1] == '-') {
								ocultaJ2[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					case 4:
						
						col1 = col1+1;
						
						if(col1 < max && col1 >= 0) {
							if(ocultaJ2[fila1][col1] == '-') {
								ocultaJ2[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					default:
						System.out.println("Opció incorrecta");
					}

				} while (op != 0);
			}
		}
		
		}while(correcte == false);
		
	}

	private static void ColocarFragata(char[][] ocultaJ1, int fila1, int col1, int max) {
		// TODO Auto-generated method stub
		boolean correcte = false;
		int op = 0;
		
		do {
		System.out.print("X: ");fila1 = src.nextInt();
		System.out.print("Y: ");col1 = src.nextInt();
		
		if(fila1 < max && col1 < max) {
			if(ocultaJ1[fila1][col1] == '-') {
				ocultaJ1[fila1][col1] = 'X';
				
				System.out.println("En quina direcció vols que vagi ?");
				
				do {
					op = mostrarMenú();

					switch (op) {
					case 1:
						fila1 = fila1 -1;
						
						if(fila1 < max && fila1 >= 0) {
							if(ocultaJ1[fila1][col1] == '-') {
								ocultaJ1[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					case 2:
						
						fila1 = fila1+1;
						
						if(fila1 < max && fila1 >= 0) {
							if(ocultaJ1[fila1][col1] == '-') {
								ocultaJ1[fila1][col1] = 'X';
								correcte =  true;
								}
						}	
						
						break;
					case 3:
						
						col1 = col1-1;
						
						if(col1 < max && col1 >= 0) {
							if(ocultaJ1[fila1][col1] == '-') {
								ocultaJ1[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					case 4:
						
						col1 = col1+1;
						
						if(col1 < max && col1 >= 0) {
							if(ocultaJ1[fila1][col1] == '-') {
								ocultaJ1[fila1][col1] = 'X';
								correcte =  true;
								}
						}
						
						break;
					default:
						System.out.println("Opció incorrecta");
					}

				} while (op != 0);
			}
		}
		
		}while(correcte == false);
		
	}

	private static int mostrarMenú() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean correcte = true;

		System.out.println("1. Amunt");
		System.out.println("2. A sota");
		System.out.println("3. A l'esqurra");
		System.out.println("4. A la dreta");
		
		do {
			System.out.print("Opció: ");
			try {
				op = src.nextInt();
			} catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}

		} while (correcte == false);
		
		
		return op;
	}

	private static void VerEstado(char[][] visibleJ1, char[][] visibleJ2, String jugador1, String jugador2, int puntsj2,
			int puntsj1, int max) {
		// TODO Auto-generated method stub
		System.out.println("Estat del jugador1 " + jugador1 +" amb " + puntsj1 + " punts.");
		for(int i = 0; i < max; i++) {
			for(int j = 0; j < max; j++) {
				System.out.print(visibleJ1[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println("Estat del jugador2 " + jugador2 +" amb " + puntsj2 + " punts.");
		for(int i = 0; i < max; i++) {
			for(int j = 0; j < max; j++) {
				System.out.print(visibleJ2[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static char[][] OmplirTaulerBuit(int max) {
		// TODO Auto-generated method stub
		char[][] tauler = new char[max][max] ;
		
		for(int i = 0; i < max; i++) {
			for(int j = 0; j < max; j++) {
				tauler[i][j] = '-';
			}
		}
		
		return tauler;
	}
}