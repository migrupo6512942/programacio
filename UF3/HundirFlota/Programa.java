package Modular.HundirFlota;

import java.util.Scanner;
/**
 * Classe que llança l'aplicació del penjat. És un distribuidor de tasques
 * mitjançant un menú d'opcions
 */

public class Programa {

	/**
	 * 1.- Mostrar Ajuda 2.- Definir Jugador 3.- Jugar Partida 4.- Veure Jugador 0.- Sortir
	 */

	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op = 0; // guarda l'opció escollida per l'usuari
		String jugador1 = null; // guarda el nom del jugador actual
		String jugador2 = null; 
		boolean resultat;
		boolean definit = false;

		do {
			op = mostrarMenú();

			switch (op) {
			case 1:
				Ajuda.unaAjuda();
				break;
			case 2:
				jugador1 = Jugador.definirJugador1();
				jugador2 = Jugador.definirJugador2();
				definit = true;
				break;
				
				
			case 3:
				if (definit == true) {
					Joc.jugarPartida(jugador1, jugador2);
					definit = false; // obliguem a definir el jugador si volem jugar altra partida
				} else
					System.out.println("No pots jugar fins que no estigui definit el jugador");
				break;
				
				
			case 4:
				System.out.println("Partides guanyades per cada jugador: ");
				Jugador.VeureResultats();
				break;
			case 0:
				System.out.println("Fi de joc");
				break;
			default:
				System.out.println("Opció incorrecta");
			}

		} while (op != 0);

	}

	private static int mostrarMenú() {

		// Scanner src = new Scanner(System.in);
		// TODO Auto-generated method stub
		int op = 0;
		boolean correcte = true;

		System.out.println("Hundir la Flota");
		System.out.println("1. Mostrar Ajuda");
		System.out.println("2. Definir jugador");
		System.out.println("3. Jugar partida");
		System.out.println("4. Resultats jugador");
		System.out.println("0. Sortir");

		do {
			System.out.print("Opció: ");
			try {
				op = src.nextInt();
			} catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}

		} while (correcte == false);

		// src.close();

		return op;
	}

}