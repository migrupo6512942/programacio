package capitol1;

public class Granja {

	Gos[] gossos; // (que guardarà les dades dels gossos que tenim a la granja)
	private int numGossos;// (que serà el nombre de gossos que tenim actualment)
	private int topGossos;// (que serà el nombre màxim que podem tenir, la nostra capacitat)

	/*
	 * Fer constructor sense paràmetres que instanciï un vector de 100 gossos o
	 * ArrayList i posi numGossos a 0, Posa topGossos a 100.
	 */

	public Granja() {
		gossos = new Gos[100];
		numGossos = 0;
		topGossos = 100;
	}

	/*
	 * Fes un constructor Granja(int) que rebrà com a paràmetre un enter que indica
	 * quina és la dimensió de la nostra granja (capacitat màxima). Si aquest valor
	 * no és un enter entre 1 i 100, el canviarà a 100. Per tant, ha d’actualitzar
	 * topGossos i instanciar el vector convenientment o crear l’arrayList. De
	 * moment la granja no tindrà cap gos.
	 */

	public Granja(int capacitatMax) {
		if (capacitatMax < 1 || capacitatMax > 100) {
			capacitatMax = 100;
		}
		gossos = new Gos[capacitatMax];
		topGossos = capacitatMax;
	}

	// Fes els mètodes public getter dels atributs topGossos i numGossos. Atenció,
	// no facis els setter.

	public int getTopGossos() {
		return topGossos;
	}

	public int getNumGossos() {
		return numGossos;
	}

	/*
	 * Fes un mètode public int afegir(Gos) que treballarà així: Rebrà el nou gos
	 * que volem ficar a la granja. Si hi ha espai pel gos → afegirà el gos passat
	 * com a paràmetre a la posició numGossos i incrementarà el valor de numGossos.
	 * Retornarà numGossos Si no hi ha espai pel gos → no farà res i retornarà -1
	 */

	public int afegir(Gos nom) {

		if (topGossos != numGossos) {
			gossos[numGossos] = nom;
			numGossos++;
			return numGossos;
		} else {
			System.out.println("Error Gos no afegit");
			return -1;
		}
	}

	// Fer el mètode String toString() de la millor manera que trobis.

	public String toString() {
		return "Capacitat màxima: " + getTopGossos() + " Numero de gossos a la granja: " + getNumGossos();
	}

	private String toStringGossos() {
		String res = "";
		for (int i = 0; i < getTopGossos(); i++) {
			if (gossos[i] != null) {
				System.out.println("Nom: " + gossos[i] + "\nposició: " + i + "\n");
			}
		}
		return res;
	}

	/*
	 * Fes un mètode públic void visualitzar(). Hauria de mostrar les dades de la
	 * granja i la dels gossos que hi ha enumerats per la posició que ocupen dins de
	 * la granja. Fes servir els mètodes toString que vegis adients. Finalment ens
	 * indicarà el total de gossos que es troben.
	 */

	public void visualitzarGranja() {
		System.out.println(this.toString());
		System.out.println(this.toStringGossos());
	}

	/*
	 * Fes un mètode públic void visualitzarVius() //mostrarà únicament els gossos
	 * de la granja que estiguin VIUS.
	 */

	public void visualitzarVius() {
		for (int i = 0; i < getTopGossos(); i++) {
			if (gossos[i].getGosEstat() == GosEstat.VIU) {
				System.out.println("Nom: " + gossos[i] + " posició: " + i);
			}
		}
	}

	/*
	 * Fes un procediment void generarGranja(int totalGossos) que generi una granja
	 * amb gossos. Les dades de cada gos seran aleatòries. totalGossos indica el
	 * nombre de gossos que ficarem a la granja; no pot superar la grandària de la
	 * granja. Si fos major, col·locarem a la granja gossos fins a omplir-la.
	 */

	public void generarGranja(int totalGossos) {
		if (totalGossos > topGossos) {
			System.out.println(
					"El total de gossos a afegir supera la capacitat de la granja nomes es ficaran els que capiguin");
			totalGossos = topGossos;
		}

		for (int i = 0; i < totalGossos; i++) {
			String randomNom = "Gos" + (i + 1);
			int randomEdat = (int) (Math.random() * 10) + 1;
			char randomSexe = (Math.random() < 0.5) ? 'M' : 'F';

			Gos newGos = new Gos(randomNom, randomSexe);
			newGos.setEdat(randomEdat);

			int result = afegir(newGos);
			//numGossos++;

			if (result == -1) {
				System.out.println("Granja plena");
				break;
			}
		}
	}

	/*
	 * Fer el mètode públic Gos obtenirGos(int) que retornarà el Gos que està a la
	 * posició passada com a paràmetre. Si no hi ha cap gos (posició fora de rang),
	 * retornarà null.
	 */

	public Gos obtenirGos(int posicioGos) {

		if (posicioGos > this.getNumGossos()) {
			return null;
		}

		else if (gossos[posicioGos] != null) {
			return gossos[posicioGos];
		} else
			System.out.println("Error no s'ha trobat el Gos");
		return null;
	}
}
