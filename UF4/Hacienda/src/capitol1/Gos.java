package capitol1;

public class Gos {

//Atributs de la classe

	private int edat = 4;
	private String nom = "SenseNom";
	private int fills = 0;
	private char sexe = 'M'; // F: Famella, M: Mascle
	private Raça raça = null;
	private GosEstat estat = GosEstat.VIU;

//Atribut de la clase

	private static int numGossos = 0;

//Comportament de la classe

	public void borda() {

		System.out.println("Guau, guau!!");

	}

	public String toString() {
		String res = "Soc el gos " + getNom() + "\nEdat: " + getEdat() + "\nFills: " + getFills() + "\nSexe: "
				+ getSexe() + "\nEstat: " + getGosEstat();
		if (raça != null) {
			res = res + "\nRaça: " + getRaça();
		}
		return res;
	}

//Constructor de la classe
	public Gos() {
		nom = "Sense nom";
		edat = 4;
		fills = 0;
		sexe = 'M';
		numGossos++;
		estat = GosEstat.VIU;
	}

	public Gos(String nomG) {
		nom = nomG;
		edat = 4;
		fills = 0;
		sexe = 'M';
		estat = GosEstat.VIU;
	}

	public Gos(String nom, char sexe) {
		this(nom);
		this.sexe = sexe;
	}

	public Gos(Gos g) {
		this.nom = g.nom;
		this.edat = g.edat;
		this.fills = g.fills;
		this.sexe = g.sexe;
		this.raça = g.raça;
	}

	public Gos(String nom, char sexe, Raça raça) {
		this(nom, sexe);
		this.raça = raça;
	}

//Setter i getter de la classe 

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		if (edat >= 0) {
			this.edat = edat;

			// Comprova si té raça i si la seva edat supera l'esperança de vida de la raça
			if (raça != null && edat > raça.getTempsVida()) {
				estat = GosEstat.MORT;
			}

			// Si no té raça, comprova si la seva edat supera els 10 anys
			if (raça == null && edat > 10) {
				estat = GosEstat.MORT;
			}
		}
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		if (fills >= 0)
			this.fills = fills;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		if (sexe == 'M' || sexe == 'F')
			this.sexe = sexe;
	}

	public Raça getRaça() {
		return raça;
	}

	public void setRaça(Raça raça) {
		this.raça = raça;
	}

	public GosEstat getGosEstat() {
		return estat;
	}

	public void setGosEstat(GosEstat estat) {
		this.estat = estat;
	}

	// Metode toStrig

	public void visualitzarGos() {
		System.out.println(this);

	}

	public void clonar(Gos g) {
		this.nom = g.nom;
		this.edat = g.edat;
		this.fills = g.fills;
		this.sexe = g.sexe;
		numGossos++;
	}

	// Geter de numGossos -- devuelve el numero de gossos
	public static int quantitatGossos() {
		return numGossos;
	}

	// Implementa, a la classe Gos, el mètode públic Gos aparellar(Gos g);
	public Gos GosAparellar(Gos g) {

		if (this.sexe != g.sexe && this.edat >= 2 && this.edat <= 10 && g.edat >= 2 && g.edat <= 10
				&& this.estat == GosEstat.VIU && g.estat == GosEstat.VIU) {
			/* this = hembra */ if (this.sexe == 'F' && this.fills < 3) {

				if (this.getRaça() == null && g.getRaça() == null) {
					int Edat = 0;
					char randomSexe = generarSexe(g);
					Raça raça = null;
					String nom = comprobarNom(randomSexe, g);
					Gos GosFill = new Gos(nom, randomSexe);
					generarGosFill(GosFill, Edat, raça, g);
					return GosFill;

				} else if (this.getRaça() != null && g.getRaça() == null) {
					g.setRaça(this.getRaça());
				} else if (this.getRaça() == null && g.getRaça() != null) {
					this.setRaça(g.getRaça());
				}

				if (this.raça.getGosMida() == g.raça.getGosMida()
						|| this.raça.getGosMida() == GosMida.MITJA && g.raça.getGosMida() == GosMida.PETIT
						|| this.raça.getGosMida() == GosMida.GRAN && g.raça.getGosMida() == GosMida.PETIT
						|| this.raça.getGosMida() == GosMida.GRAN && g.raça.getGosMida() == GosMida.MITJA) {

					int Edat = 0;
					char randomSexe = generarSexe(g);
					String nom = comprobarNom(randomSexe, g);
					Raça raça = comprobarDominancia(g);

					Gos GosFill = new Gos(nom, randomSexe);
					generarGosFill(GosFill, Edat, raça, g);
					return GosFill;

				} else
					return null;
			} /* g = hembra */else if (g.sexe == 'F' && this.fills < 3) {

				if (this.getRaça() == null && g.getRaça() == null) {
					int Edat = 0;
					char randomSexe = generarSexe(g);
					String nom = comprobarNom(randomSexe, g);

					Gos GosFill = new Gos(nom, randomSexe);
					generarGosFill(GosFill, Edat, raça, g);
					return GosFill;
				} else if (this.getRaça() != null && g.getRaça() == null) {
					g.setRaça(this.getRaça());
				} else if (this.getRaça() == null && g.getRaça() != null) {
					this.setRaça(g.getRaça());
				}

				if (g.raça.getGosMida() == this.raça.getGosMida()
						|| g.raça.getGosMida() == GosMida.MITJA && this.raça.getGosMida() == GosMida.PETIT
						|| g.raça.getGosMida() == GosMida.GRAN && this.raça.getGosMida() == GosMida.PETIT
						|| g.raça.getGosMida() == GosMida.GRAN && this.raça.getGosMida() == GosMida.MITJA) {

					Raça raça = comprobarDominancia(g);
					int Edat = 0;
					char randomSexe = generarSexe(g);
					String nom = comprobarNom(randomSexe, g);

					Gos GosFill = new Gos(nom, randomSexe);
					generarGosFill(GosFill, Edat, raça, g);
					return GosFill;
				} else
					return null;
			} else {
				return null;
			}
		} else
			return null;
	}

	private Raça comprobarDominancia(Gos g) {
		// TODO Auto-generated method stub
		Raça raça = null;
		if (this.sexe == 'M') {
			if (g.raça.getDominant() == true && this.raça.getDominant() == false) {
				raça = g.getRaça();
			} else
				raça = this.getRaça();
		} else {
			if (this.raça.getDominant() == true && this.raça.getDominant() == false) {
				raça = this.getRaça();
			} else
				raça = g.getRaça();
		}
		return raça;
	}

	private void generarGosFill(Gos GosFill, int Edat, Raça raça, Gos g) {
		// TODO Auto-generated method stub
		GosFill.setEdat(Edat);
		this.setFills(this.getFills() + 1);
		g.setFills(g.getFills() + 1);
		GosFill.setRaça(raça);
	}

	private char generarSexe(Gos g) {
		// TODO Auto-generated method stub
		char sexe = (Math.random() < 0.5) ? 'M' : 'F';
		return sexe;
	}

	private String comprobarNom(char randomSexe, Gos g) {
		// TODO Auto-generated method stub
		String nombre =  null;

		if (g.sexe == 'M') {
			if (randomSexe == 'M') {
				return nombre = "Fill de " + g.getNom();
			} else
				return nombre = "Fill de " + this.getNom();
		} else {
			if (randomSexe == 'M') {
				return nombre = "Fill de " + this.getNom();
			} else
				return nombre = "Fill de " + g.getNom();
		}
	};
}