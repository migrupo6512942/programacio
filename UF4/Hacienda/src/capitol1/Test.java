package capitol1;

public class Test {

	public static void main(String[] args) {
// TODO Auto-generated method stub

		// Al programa principal es demanarà un número entre 1 i 20, que seran els anys
		// o cicles que es volen controlar.

		int cicles = 10	;

		// ii) Generar una granja amb 10 gossos.

		Granja granja1 = new Granja(50);
		granja1.generarGranja(10);
		granja1.visualitzarGranja();

		// Fer un bucle de <<cicles>> vegades on es simularà el pas del temps. A cada
		// volta i per cada gos de la granja

		for (int i = 0; i < cicles-1; i++) {
			Gos GosFill = granja1.obtenirGos(i).GosAparellar(granja1.obtenirGos(i + 1));

			if (GosFill != null) {
				System.out.println("Ha nascut un gos!!!!!!!");
				granja1.afegir(GosFill);
				int edad1 = granja1.obtenirGos(i).getEdat();
				granja1.obtenirGos(i).setEdat(edad1 + 1);

				int edad2 = granja1.obtenirGos(i + 1).getEdat();
				granja1.obtenirGos(i + 1).setEdat(edad2 + 1);
				i++;
			} else if (GosFill == null){
				//intercambiar posicio
				Gos gos1 = granja1.obtenirGos(i);
				Gos gos2 = granja1.obtenirGos(i+1);
				
				granja1.gossos[i] = gos2;
				granja1.gossos[i+1] = gos1;
				
				int edad1 = granja1.obtenirGos(i).getEdat();
				granja1.obtenirGos(i).setEdat(edad1 + 1);
			}
			granja1.visualitzarGranja();
			System.out.println("cicle " + i);
		}

		/*
		 * // Generarà una granja de gossos amb 5 gossos (granja1) Granja granja1 = new
		 * Granja(5); granja1.generarGranja(5); Raça pastorAlemany = new
		 * Raça("Pastor Alemany", GosMida.GRAN, 14, true);
		 * granja1.obtenirGos(0).setRaça(pastorAlemany); Granja granja2 = new Granja(5);
		 * granja2.generarGranja(5); Raça Pitbull = new Raça("Pitbull", GosMida.GRAN,
		 * 10, true); granja2.obtenirGos(0).setRaça(Pitbull);
		 * 
		 * Granja granja3 = new Granja(5);
		 * 
		 * for (int i = 0; i < granja1.getTopGossos(); i++) { Gos g1 =
		 * granja1.obtenirGos(i); Gos g2 = granja2.obtenirGos(i); Gos g3 =
		 * g1.GosAparellar(g2); if (g3 != null) { granja3.afegir(g3); } }
		 * 
		 * granja1.visualitzarGranja(); granja2.visualitzarGranja();
		 * granja3.visualitzarGranja();
		 */

		/*
		 * // creï una granja,
		 * 
		 * // Granja granja1 = new Granja();
		 * 
		 * // demani a l’usuari el nombre de gossos (totalGossos) que es volen ficar i
		 * // ompli la granja amb totalGossos,
		 * 
		 * // granja1 = new Granja(3);
		 * 
		 * // mostri el contingut de la granja per pantalla.
		 * 
		 * // granja1.generarGranja(3);
		 * 
		 * // granja1.visualitzarGranja();
		 */

		/*
		 * En un bucle, fins que l’usuari vulgui continuar demanarà a l’usuari la
		 * posició del gos que vol tractar, mostrarà les dades del gos i preguntarà quin
		 * atribut vol modificar: l’estat o l’edat, agafarà el nou valor i actualitzarà
		 * l’atribut d’aquell gos.
		 */

		/*
		 * boolean continuar = true; while(continuar) {
		 * System.out.println("Posició del gos que vol tractar: "); int pos = 0;
		 * granja1.obtenirGos(pos);
		 * System.out.println("Que atribut vols modificar: estat o edat"); String
		 * modificar = "estat"; if(modificar == "estat") { GosEstat e1 = GosEstat.MORT;
		 * Gos gos = granja1.obtenirGos(pos); gos.setGosEstat(e1); continuar = false; }
		 * else if (modificar == "edat") { int edad = 2; Gos gos =
		 * granja1.obtenirGos(pos); gos.setEdat(edad); continuar = false; } else
		 * {System.out.println("Error");};
		 * 
		 * }
		 * 
		 * //Finalment, es tornarà a mostrar el contingut de la granja
		 * 
		 * granja1.visualitzarGranja();
		 */
		/*
		 * Raça pastorAlemany = new Raça("Pastor Alemany", GosMida.GRAN, 14, false); Gos
		 * g = new Gos("Sonia",'F',pastorAlemany); Gos g2 = new Gos("Carla",'M');
		 * g2.setFills(10);
		 * 
		 * //g.borda(); g.visualitzar(); g2.visualitzar();
		 */

		/*
		 * System.out.println("Gos: " + g); g2.borda(); System.out.println("Gos: " + g);
		 * if (g.getSexe() == g2.getSexe()) {
		 * System.out.println("Els dos son del mateix sexe"); }
		 * System.out.println("Gossos nascuts: " + Gos.quantitatGossos());
		 */
	}

}