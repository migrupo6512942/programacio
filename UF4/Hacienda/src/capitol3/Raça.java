package capitol3;

public class Raça {
	// atrb de la clase

	private String nomRaça;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;

	// Constructor amb tres paràmetres
	public Raça(String nom, GosMida gt, int t, boolean domina) {
		// Crida al constructor amb dos paràmetres
		nomRaça = nom;
		mida = gt;
		tempsVida = t;
		dominant = domina;
	}

	// Constructor amb dos paràmetres
	public Raça(String nom, GosMida mida) {
		this.nomRaça = nom;
		this.mida = mida;
		this.tempsVida = 10; // Valor per defecte
		this.dominant = false; // Valor per defecte
	}

	// Setter y getter de la clase

	public String getNom() {
		return nomRaça;
	}

	public void setNom(String nomRaça) {
		this.nomRaça = nomRaça;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		if (tempsVida >= 0 && tempsVida <= 10)
			this.tempsVida = tempsVida;
	}

	public boolean getDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}

	public GosMida getGosMida() {
		return mida;
	}

	public void setGosMida(GosMida mida) {
		this.mida = mida;
	}

	// Metode toString

	public void visualitzarRaça() {
		System.out.println(this.toString());
	}

	public String toString() {
		return getNom() + " Temps Vida: " + getTempsVida() + " GosMida: " + getGosMida() + " Dominant: "
				+ getDominant();
	}

	public GosMida major(Raça r1, Raça r2) {

		GosMida m = null;

		switch (r1.mida) {
		case GRAN:
			m = GosMida.GRAN;
			break;
		case MITJA:
			if (r2.mida == GosMida.GRAN) {
				m = r2.mida;
			} else
				m = r1.mida;
			break;
		default:
			break;
		}

		return m;
	}
}
