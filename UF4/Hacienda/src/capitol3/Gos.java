package capitol3;

public class Gos extends Animal {

//Atributs de la classe

	private Raça raça;

//Atribut de la clase

	private static int numGossos = 0;

//Comportament de la classe
@Override
	public void so() {

		System.out.println("Guau, guau!!");

	}

//Constructor de la classe
	public Gos() {
		super();
		this.raça = null;
	}

	public Gos(String nom, int edat, Raça raça, AnimalSexe sexe) {
		super(nom, edat, sexe);
		this.raça = raça;
	}

	public Gos(String n) {
		super(n);
		raça = null;
	}

	public Gos(Gos g) {
		this();
		clonar(g);
	}

	// Setter i Getter

	public Raça getRaça() {
		return raça;
	}

	public void setRaça(Raça raça) {
		this.raça = raça;
	}

	// Metode toStrig
	@Override
	public String toString() {
		String res = "";

		res = "Gos: " + super.toString();

		if (raça != null) {
			res += " Raça: " + raça.getNom();
		} else
			res += " Raça desconeguda";

		return (res);
	}

	public void visualitzarGos() {
		System.out.println(this);

	}
	
	
	public void clonar(Gos g) {
		super.clonar(g);
		raça = g.getRaça();
	}

	// Incrementar Edad
	@Override
	public void incEdat() {
		if (this.estat == AnimalEstat.VIU) {
			super.setEdat(super.getEdat() + 1);
			;

			if (this.raça == null) {
				if (super.getEdat() >= 10) {
					this.estat = AnimalEstat.MORT;
				}
			} else if (this.raça != null && super.getEdat() > raça.getTempsVida()) {
				this.estat = AnimalEstat.MORT;
			}

		}
	}

	// Geter de numGossos -- devuelve el numero de gossos
	public static int quantitatGossos() {
		return numGossos;
	}

	// Implementa, a la classe Gos, el mètode públic Gos aparellar(Gos g);
	@Override
	public Animal aparellar(Animal g) {
		// TODO Auto-generated method stub
		
		Gos fill = null;
		int qSexe;
		
		//veriquem que son dos gossos
		if (g instanceof Gos == false)
			return null;
		
		// comprovem que estan vius
		if (this.getEstat()!= AnimalEstat.VIU || g.getEstat()!= AnimalEstat.VIU)
			return null;
		
		// revisem que no siguin del mateix sexe
		if (this.getSexe() == g.getSexe()) 
			return null;
		
		//generaci� del fill
		fill = new Gos();
		fill.setEdat(0);
		fill.setNom("Fill de " + this.getNom() + "/" + g.getNom());
		fill.setRaça(this.getRaça());
		 
		//incrementar n�mero de fills dels pares
		this.incFills();
		g.incFills();
		
			
		return fill;
	}

}