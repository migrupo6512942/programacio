package capitol3;

public class Ganso extends Animal {
	private GansoTipus tipus;
	private static int edadTope = 6;

	Ganso() {
		super();
		edadTope = 6;
		tipus = GansoTipus.DESCONEGUT;
	}

	Ganso(Ganso g) {
		this();
		clonar(g);
	}

	// Comportament de la classe
	@Override
	public void so() {
		System.out.println("*Sonido de ganso*!!");
	}

	// Implementa, a la classe Ganso, el mètode públic Gos aparellar(Gos g);
	@Override
	public Animal aparellar(Animal g) {
		// TODO Auto-generated method stub

		Ganso fill = null;
		int qSexe;

		// veriquem que son dos gossos
		if (g instanceof Gos == false)
			return null;

		// comprovem que estan vius
		if (this.getEstat() != AnimalEstat.VIU || g.getEstat() != AnimalEstat.VIU)
			return null;

		// revisem que no siguin del mateix sexe
		if (this.getSexe() == g.getSexe())
			return null;

		// generaci� del fill
		fill = new Ganso();
		fill.setEdat(0);
		fill.setNom("Fill de " + this.getNom() + "/" + g.getNom());

		// incrementar n�mero de fills dels pares
		this.incFills();
		g.incFills();

		return fill;
	}

	// Incrementar Edad
	@Override
	public void incEdat() {
		if (this.estat == AnimalEstat.VIU) {
			super.setEdat(super.getEdat() + 1);

			if (this.tipus == GansoTipus.AGRESSIU && super.getEdat() == edadTope-1) {
				this.estat = AnimalEstat.MORT;
			}
			else if(this.tipus == GansoTipus.DOMESTIC && super.getEdat() == edadTope+1) {
				this.estat = AnimalEstat.MORT;
			}
			else if (this.tipus == GansoTipus.DESCONEGUT && super.getEdat() == edadTope) {
				this.estat = AnimalEstat.MORT;
			}

		}
	}
}
