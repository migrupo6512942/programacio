package capitol3;

import java.util.Random;

public class Animal {
	
	private static Random rnd = new Random();
	
	protected int edat;
	protected String nom;
	protected int fills;
	protected AnimalSexe sexe;
	protected AnimalEstat estat;
	protected static int numAnimals = 0;

	public Animal() {
		nom = "sense nom";
		edat = 0;
		fills = 0;
		switch(rnd.nextInt(2)) {
		case 0: sexe = AnimalSexe.Famella; 	break;
		case 1: sexe = AnimalSexe.Mascle; 	break;
	}
		estat = AnimalEstat.VIU;
		numAnimals++;
	}
	
	public Animal(String n) {
		this.nom = n;
	}
	
	public Animal(String nom2, int edat2, AnimalSexe sexe2) {
		this.nom = nom2;
		this.edat = edat2;
		this.sexe = sexe2;
	}

	// Getters i Setters

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		if (edat >= 0) {
			this.edat = edat;
		}
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		if (fills >= 0)
			this.fills = fills;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public AnimalEstat getEstat() {
		return this.estat;
	}

	// Metode parlar

	public void so() {
		System.out.println("hola hola");
	}

	// Metode Aparellar

	public Animal aparellar(Animal a2) {
		if (this.sexe != a2.sexe && this.estat == AnimalEstat.VIU && a2.estat == AnimalEstat.VIU) {
			Animal fill = new Animal();
			return fill;
		} else
			return null;
	}

	// Metode clonar

	public void clonar(Animal a) {
		this.nom = a.nom;
		this.edat = a.edat;
		this.fills = a.fills;
		this.sexe = a.sexe;
		numAnimals++;
	}

	// ToString + Visualitzar

	public String toString() {
		String res = "Soc l'animal " + getNom() + "\nEdat: " + getEdat() + "\nFills: " + getFills() + "\nSexe: "
				+ getSexe() + "\nEstat: " + getEstat();
		return res;
	}

	public void visualitzar() {
		System.out.println(this);

	}
	
	//Metode incEdat
	
	public void incEdat() {
		if(this.estat == AnimalEstat.VIU) {
			this.setEdat(this.getEdat()+1);
		}
	}

	public void incFills() {
		// TODO Auto-generated method stub
		int fills = this.getFills();
		this.setFills(fills+1);
	}

	public AnimalSexe getSexe() {
		// TODO Auto-generated method stub
		return sexe;
	}

	public void setSexe(AnimalSexe sexe) {
		this.sexe = sexe;
	}

}