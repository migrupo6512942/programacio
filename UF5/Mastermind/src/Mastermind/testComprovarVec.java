package Mastermind;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class testComprovarVec {

    private ArrayList<Integer> ocult;
    private int[] digits;
    private int[] resultats;

    private static ArrayList<Integer> ocult1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    private static int[] digits1 = {1, 2, 5, 6};
    private static int[] res1 = {2, 2};

    private static ArrayList<Integer> ocult2 = new ArrayList<>(Arrays.asList(5, 6, 7, 8));
    private static int[] digits2 = {5, 6, 7, 8};
    private static int[] res2 = {4, 4};

    private static ArrayList<Integer> ocult3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    private static int[] digits3 = {9, 8, 7, 6};
    private static int[] res3 = {0, 0};

    private static ArrayList<Integer> ocult4 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    private static int[] digits4 = {1, 2, 3, 4};
    private static int[] res4 = {4, 4};

    private static ArrayList<Integer> ocult5 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    private static int[] digits5 = {0, 1, 2, 3};
    private static int[] res5 = {0, 3};

    private static ArrayList<Integer> ocult6 = new ArrayList<>(Arrays.asList(4, 3, 2, 1));
    private static int[] digits6 = {3, 4, 1, 2};
    private static int[] res6 = {0, 4};

    private static ArrayList<Integer> ocult7 = new ArrayList<>(Arrays.asList(7, 8, 9, 0));
    private static int[] digits7 = {1, 2, 3, 4};
    private static int[] res7 = {0, 0};

    private static ArrayList<Integer> ocult8 = new ArrayList<>(Arrays.asList(2, 5, 6, 8));
    private static int[] digits8 = {2, 5, 6, 8};
    private static int[] res8 = {4, 4};

    private static ArrayList<Integer> ocult9 = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
    private static int[] digits9 = {5, 6, 7, 8};
    private static int[] res9 = {0, 0};

    private static ArrayList<Integer> ocult10 = new ArrayList<>(Arrays.asList(9, 8, 7, 6));
    private static int[] digits10 = {1, 2, 3, 4};
    private static int[] res10 = {0, 0};

    public testComprovarVec(ArrayList<Integer> ocult, int[] digits, int[] resultats) {
        this.ocult = ocult;
        this.digits = digits;
        this.resultats = resultats;
    }

    @Parameters
    public static Collection<Object[]> datos() {
        return Arrays.asList(new Object[][] {
            {ocult1, digits1, res1},
            {ocult2, digits2, res2},
            {ocult3, digits3, res3},
            {ocult4, digits4, res4},
            {ocult5, digits5, res5},
            {ocult6, digits6, res6},
            {ocult7, digits7, res7},
            {ocult8, digits8, res8},
            {ocult9, digits9, res9},
            {ocult10, digits10, res10}
        });
    }

    @Test
    public void testComprovarVec() {
        int[] res = Joc.comprovarVec(ocult, digits);
        assertArrayEquals(resultats, res);
    }
}