package Mastermind;

import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

/**
 * Classe que llança l'aplicació del penjat. És un distribuidor de tasques
 * mitjançant un menú d'opcions
 * 
 * This class launches the Mastermind application. It acts as a task distributor
 * through a menu of options.
 * 
 * @author deivi
 */
public class Programa {

    static Scanner src = new Scanner(System.in);

    /**
     * Mètode principal que executa el programa del Mastermind.
     * 
     * @param args Arguments de la línia de comandes (no utilitzats en aquest cas).
     */
    public static void main(String[] args) {
        int op = 0; // guarda l'opció escollida per l'usuari
        boolean resultat;
        int max = 5;
        int pWin = 0;
        int pLose = 0;
        ArrayList<Integer> ocult = new ArrayList<Integer>();

        do {
            op = mostrarMenú();

            switch (op) {
                case 1:
                    ocult = Joc.generarCombinacio(max);
                    break;
                case 2:
                    resultat = Joc.JugarPartida(ocult, max);
                    if (!resultat) {
                        pLose++;
                    } else {
                        pWin++;
                    }
                    break;
                case 3:
                    Joc.veureResultats(pLose, pWin);
                    break;
                case 0:
                    System.out.println("Fi de joc");
                    break;
                default:
                    System.out.println("Opció incorrecta");
            }

        } while (op != 0);

    }

    /**
     * Serveix per verificar l'entrada de dades
     * 
     * Checks the input data.
     * 
     * @param correcte Indica si les dades són correctes o no.
     * @return El valor de l'opció verificada.
     */
    private static int comprobarDatos(boolean correcte) {
        int df = 0;

        do {
            System.out.print("Opció: ");
            try {
                df = src.nextInt();
            } catch (Exception e) {
                System.out.println("Error, opció no vàlida");
                src.nextLine();
                correcte = false;
            }

        } while (correcte == false);

        return df;
    }

    /**
     * Serveix per mostrar el menu d'opcions del menu principal
     * 
     * Displays the menu of options for the main menu.
     * 
     * @return L'opció seleccionada pel usuari.
     */
    private static int mostrarMenú() {
        int op = 0;
        boolean correcte = true;

        System.out.println("MasterMind");
        System.out.println("1. Generar Combinació");
        System.out.println("2. Jugar partida");
        System.out.println("3. Resultats jugador");
        System.out.println("0. Sortir");

        op = comprobarDatos(correcte);

        return op;
    }

}
