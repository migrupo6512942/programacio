package Mastermind;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class testComprovarEntrada {

    private String entrada;
    private boolean resultadoEsperado;

    public testComprovarEntrada(String entrada, boolean resultadoEsperado) {
        this.entrada = entrada;
        this.resultadoEsperado = resultadoEsperado;
    }

    @Parameters
    public static Collection<Object[]> datosEntrada() {
        return Arrays.asList(new Object[][] {
            {"1234", true},    
            {"1123", false},   
            {"abcd", false},   
            {"12345", false},  
            {"12ab", false},
            {"5678", true},
            {"9999", true},
            {"0a1b", false},
            {"8765", true},
            {"1a2b", false}
        });
    }

    @Test
    public void testComprovarEntrada() {
        boolean resultado = Joc.comprovarEntrada(entrada);
        assertEquals(resultadoEsperado, resultado);
    }
}
