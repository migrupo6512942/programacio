package Mastermind;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * La clase Joc representa el juego Mastermind.
 */
public class Joc {

	/**
	 * Genera una combinación aleatoria de dígitos sin repetición.
	 *
	 * @param max El número de dígitos en la combinación.
	 * @return Una lista que representa la combinación generada.
	 */
	public static ArrayList<Integer> generarCombinacio(int max) {
		ArrayList<Integer> ocult = new ArrayList<Integer>();
		ArrayList<Integer> Nums = new ArrayList<Integer>();
		Nums.add(0);
		Nums.add(1);
		Nums.add(2);
		Nums.add(3);
		Nums.add(4);
		Nums.add(5);
		Nums.add(6);
		Nums.add(7);
		Nums.add(8);
		Nums.add(9);
		int cont = 0;

		do {
			int x = (int) (Math.random() * 10);

			if (Nums.contains(x)) {
				int pos = Nums.indexOf(x);
				Nums.remove(pos);
				ocult.add(x);
				cont++;
			}

		} while (cont < max - 1);

		return ocult;
	}

	/**
	 * Juega la partida principal y verifica el resultado del juego.
	 *
	 * @param ocult La combinación oculta que el jugador debe adivinar.
	 * @param max   El número de dígitos en la combinación.
	 * @return true si el jugador adivina la combinación, false de lo contrario.
	 */
	public static boolean JugarPartida(ArrayList<Integer> ocult, int max) {
		Scanner src = new Scanner(System.in);
		int[] digits = new int[4];
		boolean acaba = false;
		int[] resultats = new int [2];
		int torn = 0;
		int cont = 0;
		

		do {

			String ent = introduirDades();
			digits = datosAlVector(ent);
			cont = 0;
			resultats = comprovarVec(ocult, digits);

			System.out.println("Has aconseguit " + resultats[0] + " plenos.");
			System.out.println("Has aconseguit adivinar " + resultats[1] + " digit.");

			torn++;

			if (resultats[0] == 4) {
				acaba = true;
			}

		} while (acaba == false && torn != 15);

		if (torn == 15) {
			System.out.println("Has superat el numero de intents disponibles la solució era " + ocult);
		} else {
			System.out.println("Enhorabona has adivinat els 4 digits!");
		}

		return acaba;
	}

	static int[] comprovarVec(ArrayList<Integer> ocult, int[] digits) {
	    int pleno = 0;
	    int adivinat = 0;

	    for (int i = 0; i < digits.length; i++) {
	        if (ocult.contains(digits[i])) {
	            int indexInOcult = ocult.indexOf(digits[i]);

	            if (indexInOcult == i) {
	                pleno++;
	                adivinat++;
	            } else {
	                adivinat++;
	            }

	        }
	    }

	    int[] resultats = {pleno, adivinat};
	    return resultats;
	}

	public static int[] datosAlVector(String ent) {
		// TODO Auto-generated method stub
		int[] digits = new int[4];
		int cont = 0;

		do {
			digits[cont] = Character.getNumericValue(ent.charAt(cont));
			cont++;
		} while (cont < 4);

		return digits;
	}

	/**
	 * Introduce los datos para intentar adivinar el número aleatorio y comprueba
	 * los errores en la introducción de datos.
	 *
	 * @return La cadena que representa el intento del jugador.
	 */
	public static String introduirDades() {
		Scanner src = new Scanner(System.in);
		boolean correcte = false;

		System.out.println("Introdueix el teu intent(4 digits): ");
		String ent = src.nextLine();
		do {
			correcte = comprovarEntrada(ent);
			if (!correcte) {
				ent = introduirDades();
			}
		} while (!correcte);

		/*
		 * do { if (ent.length() != 4) {
		 * System.out.println("Error. Introdueix el teu intent(4 digits): "); ent =
		 * src.nextLine(); }
		 * 
		 * } while (ent.length() != 4);
		 */

		return ent;
	}

	static boolean comprovarEntrada(String ent) {
	    boolean correcte = true;

	    if (ent == null || ent.length() != 4) {
	        correcte = false;
	    } else if (!Repetidos(ent)) {
	        correcte = false;
	    } else if (correcte) {
	        correcte = sonNumeros(ent);
	    }

	    return correcte;
	}


	private static boolean sonNumeros(String ent) {
	    for (char c : ent.toCharArray()) {
	        if (!Character.isDigit(c)) {
	            return false; // Si encuentra un carácter que no es un dígito, devuelve falso
	        }
	    }
	    return true; // Todos los caracteres son dígitos
	}

		static boolean Repetidos(String ent) {
			for (int i = 0; i < ent.length() - 1; i++) {
				for (int j = i + 1; j < ent.length(); j++) {
					if (ent.charAt(i) == ent.charAt(j)) {
						return false;
					}
				}
			}
			return true;
		}

	/**
	 * Muestra el número de partidas ganadas y de partidas perdidas.
	 *
	 * @param pLose Número de partidas perdidas.
	 * @param pWin  Número de partidas ganadas.
	 * @return
	 */
	public static String veureResultats(int pLose, int pWin) {
		System.out.println("Portes " + pWin + " partides guanyades y " + pLose + " partides perdudes.");
		String resultat = "Portes " + pWin + " partides guanyades y " + pLose + " partides perdudes.";

		return resultat;
	}

}