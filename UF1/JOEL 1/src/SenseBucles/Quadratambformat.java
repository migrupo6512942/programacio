package SenseBucles;

import java.util.Locale;
import java.util.Scanner;

public class Quadratambformat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		double costat;
		costat = src.nextDouble();
		
		double area;
		area = (costat * costat);
		
		System.out.printf(Locale.US,"%015.3f\n", area);
	}

}
