package SenseBucles;

import java.util.Scanner;

public class AviMesGran {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);

		String avi1 = src.nextLine();
		int edad1 = src.nextInt();
		src.nextLine();
		String avi2 = src.nextLine();
		int edad2 = src.nextInt();
		
		if (edad1 > edad2) {
			System.out.println(avi1);
		}
		else if (edad2 > edad1) {
			System.out.println(avi2);
		}
		else if (edad1 == edad2) {
			System.out.println("Tenen la mateixa edat");
		}
	}

}
