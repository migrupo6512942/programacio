package SenseBucles;

import java.util.Scanner;

public class croquetasdelamama {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int carn = src.nextInt();
		int croquetas = 0;
		String mesura;
		String paella = null;
		double sobrantxiqui = carn % 2.5;
		double sobrantgran = carn % 4;
		double sobrant = 0;
		
		if (sobrantxiqui < sobrantgran) {
			croquetas = (int) ((double) carn / 2.5);
			mesura = "croquetes petites";
			sobrant = sobrantxiqui;
		}
		else {
			croquetas = carn / 4;
			mesura = "croquetes grans";
			sobrant = sobrantgran;
		}
		
		if (croquetas <= 9) {
			paella = "paella petita";
		}
		else if (croquetas <= 18) {
			paella = "paella mitjana";
		}
		else if (croquetas <=36){
			paella = "paella gran";
		}
		else if (croquetas > 36) {
			paella = "paella gegant";
		}
		
		System.out.println(croquetas + " " + mesura);
		System.out.println(paella);
		System.out.println(sobrant);

	}

}
