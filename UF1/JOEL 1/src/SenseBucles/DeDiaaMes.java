package SenseBucles;

import java.util.Scanner;

public class DeDiaaMes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int dia = src.nextInt();
		int mes = 0;
		
		if (dia <= 31) {
			mes = 1;
	}
		else if (dia <= 59) {
			mes = 2;
	}
		else if (dia <= 90) {
			mes = 3;
	}
		else if (dia <= 120) {
			mes = 4;
	}
		else if (dia <= 151) {
			mes = 5;
	}
		else if (dia <= 181) {
			mes = 6;
	}
		else if (dia <= 212) {
			mes = 7;
	}
		else if (dia <= 243) {
			mes = 8;
	}
		else if (dia <= 273) {
			mes = 9;
	}
		else if (dia <= 304) {
			mes = 10;
	}
		else if (dia <= 334) {
			mes = 11;
	}
		else {
			mes = 12;
	}
	
		System.out.println(mes);
	}
}