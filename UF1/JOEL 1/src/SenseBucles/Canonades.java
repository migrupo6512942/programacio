package SenseBucles;

import java.util.Scanner;

public class Canonades {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		double metros = src.nextDouble();
		double preu = 0;
		
		if (metros < 25) {
			preu = metros * 3;
		}
		else if ((metros >= 25) && (metros <= 75)) {
			preu = (25 * 3) + ((metros - 25)*4);
		}
		else {
			preu = (25 * 3) + (50 * 4) + ((metros - 75)*5);
		}
		System.out.println(metros + "m: " + preu + " euros");
	}

}
