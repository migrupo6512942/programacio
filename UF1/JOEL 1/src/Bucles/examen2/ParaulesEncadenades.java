package Bucles.examen2;

import java.util.Scanner;

public class ParaulesEncadenades {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner src = new Scanner(System.in);
	
	// pasas a minuscula todo para evitar confusiones. Dsps lees palabra empeizas bucle cojes la ultima letra lees otra entrada y cojes la primera y entonces comparas los caractes.

		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		src.next();
		String entrada; // Guarda cada linea que llegeix
		int cadena = 0;
		
		while (casos > 0) {
			
			entrada = src.nextLine();
			char lletrafi = (char) entrada.length();
			char lletra1 = entrada.charAt(0);
			char fianterior = lletrafi;
			
			
			if (lletrafi == lletra1) {
				cadena ++;
			}
			else {
				cadena = 0;
			}
			
			casos = casos - 1; // casos -- ;
		}
		
		System.out.println(cadena);

	}
}
