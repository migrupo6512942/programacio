package Bucles.examen2;
import java.util.Scanner;

public class PerquefaFred {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		float entrada; // Guarda cada linea que llegeix
		float fred = 0;
		float mitjana = 0;
		
		while (src.hasNext()) {
			// comença el tractament d'un cas de prova
			entrada = src.nextFloat();
			if(entrada < 0 ) {
				fred ++;
				mitjana = mitjana + entrada;
			}
}
		
		if (fred > 0) {
		
			System.out.println((int)fred);
			System.out.print(mitjana/fred);
		}
		
	}

}
