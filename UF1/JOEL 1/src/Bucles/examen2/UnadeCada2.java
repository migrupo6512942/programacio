package Bucles.examen2;

import java.util.Scanner;

public class UnadeCada2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		String entrada; // Guarda cada linea que llegeix
		src.nextLine();
		String solucio;
		int i;
		boolean majuscula;
		
		while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextLine();
			solucio = "";
			i = 0;
			majuscula = false;
			
			while(i < entrada.length()) {
				if ((entrada.charAt(i) >= 'A' && entrada.charAt(i) <= 'Z' || (entrada.charAt(i) >= 'a' && entrada.charAt(i) <= 'z'))) {
					
					if (majuscula == true) {
						solucio = solucio + Character.toUpperCase(entrada.charAt(i));
						majuscula = false;
					}
					else {
						solucio = solucio + Character.toLowerCase(entrada.charAt(i));
						majuscula = true;
					}
				}
				else { // el caracter de la posicio i no es lletra
					
					solucio = solucio + entrada.charAt(i);
				}
				
				majuscula = !majuscula;
				i++;
			}
			
			System.out.println(solucio);
			
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
		}
	}

}
