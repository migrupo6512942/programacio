package Bucles.examen2;

import java.util.Scanner;

public class AdivinaAdivinanza {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		System.out.println("Indica el numero a esbrinar: "); entrada = src.nextInt();
		int adivina = 0;
		boolean funciona = true;
		int intentos = 0;

		while (funciona != false) {
			
			System.out.println("Indica un numero: "); adivina = src.nextInt();
			
			
			if (adivina > entrada) {
				System.out.println("Mes petit");
				intentos ++;

			}
			else if (adivina < entrada) {
				System.out.println("Mes gran");
				intentos ++;

			}
			else if (adivina == entrada) {
				intentos ++;
				funciona = false;
				System.out.println("FELICITATS, Has esbrinat el numero en: " + intentos + " veces");
			}
		}
	}

}
