package Bucles;

import java.util.Scanner;

public class sumarnegatiusipositius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		// Guarda cada linea que llegeix
		int positivos = 0;
		int negativos = 0;
		int entrada; 
		
		
		while (true) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada > 0) {
				positivos++;
			}
			else if (entrada < 0) {
				negativos++;
			}
			
			else if (entrada == 0){
				break;
			}			
		}
		
		if ( positivos > negativos) {
			System.out.println("POSITIUS");
		}
		else if (negativos > positivos) {
			System.out.println("NEGATIUS");
		}
		else if (negativos == positivos) {
			System.out.println("IGUALS");
		}
	}

}
