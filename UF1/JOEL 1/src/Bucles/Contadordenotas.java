package Bucles;

import java.util.Scanner;

public class Contadordenotas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		int notas = 0;
		float cont_mitj = 0;
		int cont_exe = 0;
		int cont_not = 0;
		int cont_bens = 0;
		int cont_sufi = 0;
		int cont_insufi = 0;
		int cont_minsufi = 0;
		
		while (true) {
			
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada == -1) {
				break;
			}
			if (entrada >= 0 && entrada <=10) {
				notas ++;
				cont_mitj = cont_mitj + entrada;
			}
			
			if (entrada == 9 || entrada == 10) {
				cont_exe ++;
			}
			else if (entrada >= 7 && entrada <= 8) {
				cont_not ++;
			}
			else if (entrada == 6) {
				cont_bens ++;
			}
			else if (entrada == 5) {
				cont_sufi ++;
			}
			else if (entrada > 3 && entrada < 5) {
				cont_insufi ++;
			}
			else if (entrada >= 0 && entrada <= 3) {
				cont_minsufi ++;
			}
		}
		
		System.out.println("NOTES: " + notas + " MITJANA: " + (cont_mitj / notas) + " E: " + cont_exe + " N: " + cont_not + " B: " + cont_bens + " S: " + cont_sufi + " I: " + cont_insufi + " MD: " + cont_minsufi);
	}

}
