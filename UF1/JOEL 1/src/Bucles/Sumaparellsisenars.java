package Bucles;

import java.util.Scanner;

public class Sumaparellsisenars {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		int entrada; // Guarda cada linea que llegeix
		int parells = 0;
		int senars = 0;
		
		while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada%2 == 0) {
				while(entrada > 0) {
				parells = parells + entrada;
				entrada = entrada -2;
				senars = senars + entrada + 1;
				}
				System.out.println("PARELLS: " + parells + " SENARS: " + senars);
				parells = 0;
				senars = 0;
			}
			else {
				while(entrada > 0) {
				senars = senars + entrada;
				entrada = entrada -2;
				parells = parells + entrada + 1;
				}
				System.out.println("PARELLS: " + parells + " SENARS: " + senars);
				parells = 0;
				senars = 0;
			}
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
	}		
	}

}
