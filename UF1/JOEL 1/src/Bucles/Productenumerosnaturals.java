package Bucles;

import java.util.Scanner;

public class Productenumerosnaturals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		int entrada; // Guarda cada linea que llegeix
		int naturalssuma = 0;
		int naturalsprod = 1;
		
		if (casos > 0){
			while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada > 0 ) {
				while(entrada > 0) {
					naturalssuma = naturalssuma + entrada;
					naturalsprod = naturalsprod * entrada;
					entrada --;
				}
				System.out.println(" SUMA:" + naturalssuma + "PRODUCTE:" + naturalsprod );
				naturalssuma = 0;
				naturalsprod = 1;
			}
			else {
				System.out.println("ELS NOMBRES NATURALS COMENCEN EN 1");
			}
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
			}
		}
	}
}