package Bucles;

import java.util.Scanner;

public class Nota10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		int nota10 = 0;
		int notas = 0;
		int contador = 0;
		
		while (contador != -1) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada >= 0 && entrada <= 10) {
				if (entrada == 10) {
					nota10++;
					notas++;
				}
				else {
					notas++;
				}
			}
			//fi del cas de prova
			contador = entrada;

		}
		System.out.println("TOTAL NOTES: " + notas + " NOTES10: " + nota10);
	}
}