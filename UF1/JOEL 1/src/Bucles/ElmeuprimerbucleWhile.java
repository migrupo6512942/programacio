package Bucles;
import java.util.Scanner;

public class ElmeuprimerbucleWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		
		while (true) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			
			if (entrada != 0) {
				entrada ++;
			}
			
			//fi del cas de prova
			if (entrada == 0){
				break;
			}
			
			System.out.println(entrada);

		}
		
	}

}
