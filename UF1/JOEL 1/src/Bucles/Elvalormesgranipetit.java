package Bucles;

import java.util.Scanner;

public class Elvalormesgranipetit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		int min = 0;
		int max = 0;
		
		while (true) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada == 0) {
				break;
			}
			if (entrada < min || min == 0) {
				min = entrada;
			}
			if (entrada > max || max == 0) {
				max = entrada;
			}
			//fi del cas de prova
		}
		System.out.println(max + " " + min);

		
	}

}
