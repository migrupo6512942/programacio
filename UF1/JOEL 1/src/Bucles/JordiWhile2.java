package Bucles;
import java.util.Scanner;

public class JordiWhile2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int entrada; // Guarda cada linea que llegeix
		int viwers = 0;
		int strikes = 0;
		
		while (true) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada > 0) {
				viwers = viwers + entrada;
			}
			else if (entrada == -1) {
				strikes ++;
			}
			if (strikes == 3) {
				break;
			}
			//fi del cas de prova
		}
		
		System.out.println(viwers);
	}

}
