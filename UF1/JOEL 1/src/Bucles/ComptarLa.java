package Bucles;

import java.util.Scanner;

public class ComptarLa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		src.nextLine();
		String entrada; // Guarda cada linea que llegeix
		
		while (casos > 0) {
			int pos = 0;
			int num = 0;
			entrada = src.nextLine();
			while(pos < entrada.length()-1) {	
				if ((entrada.charAt(pos) == 'L') || (entrada.charAt(pos) == 'l' ) && ((entrada.charAt(pos + 1) == 'A') || (entrada.charAt(pos + 1) == 'a'))) {
						num ++;
					}
				pos ++;
				}
			
			casos = casos - 1;
			System.out.println(num);
			}		
		}
}

