package Bucles;

import java.util.Scanner;

public class ExplosionArcana2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		 // indica el numero de casos a tractar
		int entrada; // Guarda cada linea que llegeix
		int vida;
		int casos = 0;
		int daño = 0;
		entrada = src.nextInt();
		vida = src.nextInt();
		
		while (vida > 0) {
				daño = daño + (entrada * casos);
				if (vida > daño + (entrada * casos)) {
					casos ++;
				}
				else {
					vida = vida - daño;
				}
			}
		System.out.println(casos);

	}
}


