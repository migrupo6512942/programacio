package Bucles;

import java.util.Scanner;

public class Calcularfactorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		int entrada; // Guarda cada linea que llegeix
		long factorial; // Long per numeros grans. Guarda els resultats de calcular el factorial.
		
		while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			factorial = 1;
			while(entrada > 0) {
				factorial = factorial * entrada;
				entrada --;
			}
			System.out.println(factorial);
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
		}
	}

}
