package Bucles;

import java.util.Scanner;

public class ComptarVocals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		src.nextLine();
		String entrada; // Guarda cada linea que llegeix

		while (casos > 0) {
			entrada = src.nextLine();
			int pos = 0;
			boolean negreta = false;
			
			while(pos < entrada.length()) {
				if (entrada.charAt(pos) == '*'){
					negreta = true;
					pos ++;
					while (negreta != false) {
						if (entrada.charAt(pos) >= 'A' || entrada.charAt(pos) >= 'a' || entrada.charAt(pos) <= 'Z' || entrada.charAt(pos) <= 'z') {
							if (entrada.charAt(pos + 1) == '*') {
								negreta = false;
							}
						}
						else if (entrada.charAt(pos) == '*') {
							negreta = false;
							System.out.println("Negreta no correcte");
						}
					}
				}
				pos ++;
			}
			System.out.println();
		}
	}

}
