package Bucles;

import java.util.Scanner;

public class Diguespatata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		src.nextLine(); // Netejar el buffer d'entrada
		String entrada; // Guarda cada linea que llegeix
		
		while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextLine();
			System.out.println(entrada);
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
		}
		
	}

}
