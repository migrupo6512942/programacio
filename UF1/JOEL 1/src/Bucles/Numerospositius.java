package Bucles;

import java.util.Scanner;

public class Numerospositius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt(); // indica el numero de casos a tractar
		int entrada; // Guarda cada linea que llegeix
		int contador = 0;
		
		while (casos > 0) {
			// comença el tractament d'un cas de prova
			entrada = src.nextInt();
			if (entrada > 0) {
				contador ++ ;
			}
			//fi del cas de prova
			casos = casos - 1; // casos -- ;
		}
		System.out.println(contador);
	}

}
