package RangsArrays;

import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		// Crear una constant. Sempre es diu un nom en mayuscula.
		
		final int N = 8;
		
		int [] vecNums; // nomes numeros enters
		vecNums = new int [8]; // Se crea un vector. Numero de caselles que tindrà el vector, Se cuenta desde 0.
		
		// Per accedir i asignar un valor :
		
		vecNums[4] = 3;
		
		// Per veure el valor asignat
		
		// if (vecNums [4] %2 == 0)
		
		// Primer saber la grandaria del vector per asignar valors. Hem de recorrer les caselles
		
		int i ;
		i = 0;
		
		while (i < 8) {
			System.out.println("Valor de la posició: " + i);
			vecNums[i] = src.nextInt();
			i++;
		}
		
		// Printar tots els valors del vector
		while (i < 8) {
			System.out.print(vecNums[i] + "");
			i ++;
		}
	}

}
