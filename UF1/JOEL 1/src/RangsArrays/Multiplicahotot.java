package RangsArrays;

import java.util.Scanner;

public class Multiplicahotot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		while (casos > 0) {
			final int K = src.nextInt(); // Constant que da la grandaria del vector
			int[] vec; // Declara una variable de tipus vector.
			vec = new int[K]; // Creem un vector buit de k posicions
			int pos = 0;
			
			while (pos > K) {
				vec[pos] = src.nextInt();
				pos++;
			}
			
			pos = 0;
			int num = src.nextInt();
			
			while (pos < K) {
				System.out.print(vec[pos]*num + " ");
				pos++;
			}
			
			casos--;
		}		
		
	}
}

