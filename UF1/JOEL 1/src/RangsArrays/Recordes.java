package RangsArrays;

import java.util.Scanner;

public class Recordes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		while (casos > 0) {
			
			final int K = src.nextInt(); // Constant que da la grandaria del vector
		
			int[] vec; // Declara una variable de tipus vector.
			vec = new int[K]; // Creem un vector buit de k posicions
			
			// omplir el vector amb nombres que llegim d'entrada
			
			int i ;
			i = 0;
			
			while (i < K) {
				vec[i] = src.nextInt();
				i++;
			}
			
			int pos = src.nextInt();
			
			System.out.println(vec[pos] + "\n");
			casos--;
		}

	}

}
