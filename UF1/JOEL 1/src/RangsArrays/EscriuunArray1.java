package RangsArrays;

import java.util.Scanner;

public class EscriuunArray1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		final int K = src.nextInt(); // Constant que da la grandaria del vector
		
		int[] vec; // Declara una variable de tipus vector.
		vec = new int[K]; // Creem un vector buit de k posicions
		
		// omplir el vector amb nombres que llegim d'entrada
		
		int i ;
		i = 0;
		
		while (i < K) {
			vec[i] = src.nextInt();
			i++;
		}
		
		// llegir la posició del vector que ens demanen per mostrar
		
		int pos = src.nextInt();
		
		// mostrem el contingut del vector
		
		i = 0;
		
		while (i < K) {
			System.out.print(vec[i] + " ");
			i++;
		}
		
		// mostrar el contingut de pos
		
		System.out.print("\n" + vec[pos]);
	}

}
